const getCube = 4 ** 3;
console.log(`The cube of 4 is ${getCube}`)

let address = ["221B", "Baker Street", "London", "UK", "NW1 6XE"]
const [houseNumber, streetName, city, country, zip] = address
console.log(`I live at ${houseNumber} ${streetName}, ${city} ${country} ${zip}`)

let misha = {
	breed: "Siberian Husky",
	weight: "23kg",
	length: "88cm"
}

let {breed, weight, length} = misha

console.log(`Misha is a ${breed}. She weighed ${weight} with a measurement of ${length}.`)


let arrayNum = [ 42, 32, 18, 7, 31]
arrayNum.forEach(function(arrayEa){
	console.log(`${arrayEa}`)
})

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed

	}
}

const firstDog = new Dog ("Kim", 12, "Shih Tzu")
console.log(firstDog)
const secondDog = new Dog ("Chuchu", 11, "Shih Tzu")
console.log(secondDog)